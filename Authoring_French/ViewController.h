//
//  ViewController.h
//  French_practice
//
//  Created by Shuhei on 2014/10/24.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "WordParse.h"
#import "YSTapUpDownGestureRecognizer.h"
#import "FlickView.h"
#import "HintViewController.h"

@interface ViewController : UIViewController
<UIGestureRecognizerDelegate,UITextViewDelegate,HintViewControllerProtocol>{

}
@property NSMutableArray *Words;
@property (weak, nonatomic) IBOutlet UITextView *paragraph;
@property FlickView *flickview;
@property NSUInteger start;
@property NSUInteger end;
@property NSRange tapStart;
@property PFObject *content;
@property NSMutableArray *questions;
@property NSMutableArray *answers;
- (IBAction)toNextInput:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *question_field;
- (IBAction)toQuestionVIew:(id)sender;

@end