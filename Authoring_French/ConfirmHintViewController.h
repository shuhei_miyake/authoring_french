//
//  ConfirmHintViewController.h
//  Authoring_French
//
//  Created by Shuhei on 2014/11/27.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmHintViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *hint;
@property (weak, nonatomic) IBOutlet UITextView *discription;
@property (weak, nonatomic) IBOutlet UILabel *grammer_label;
@property NSString *hint_string;
@property NSString *description_string;
@property NSString *grammer_string;
@end
