//
//  MainViewController.m
//  French_practice
//
//  Created by Shuhei on 2014/11/21.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import "MainViewController.h"
#import "ViewController.h"
#import "ConfirmTabViewController.h"
#import <Parse/Parse.h>

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView.delegate = self;
    _tableView.dataSource =self;
    // Do any additional setup after loading the view.
    
    PFQuery *query = [PFQuery queryWithClassName:@"Paragraph"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
    
        if(!error){
            pargraphs = [objects mutableCopy];
            [_tableView reloadData];
        }else{
        
            
            NSLog(@"エラー");
        
        }
        
    
    }];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return pargraphs.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // セルが作成されていないか?
    if (!cell) { // yes
        // セルを作成
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    PFObject *item = [pargraphs objectAtIndex:indexPath.row];
    cell.textLabel.text = item[@"title"];
    
    
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ConfirmTabViewController *tabBar = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmTab"];
    ViewController *view1 = [[tabBar viewControllers] objectAtIndex:0];
    view1.content = pargraphs[indexPath.row];
    [self presentViewController:tabBar animated:YES completion:nil];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
