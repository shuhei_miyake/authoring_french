//
//  QuestionDetailViewController.m
//  French_practice
//
//  Created by Shuhei on 2014/11/22.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import "QuestionDetailViewController.h"
#import "WordParse.h"

@interface QuestionDetailViewController ()

@end

@implementation QuestionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _question_label.text = _question[@"question"];
    _hint.text = _question[@"hint"];
    _description_view.text = _question[@"description"];
    NSMutableArray *ranges = [[NSMutableArray alloc]init];
    _paragraph = [[NSMutableAttributedString alloc]init];
    PFQuery *query = [PFQuery queryWithClassName:@"Answer"];
    [query whereKey:@"question" equalTo:_question];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        for(PFObject *item in objects){
            [ranges addObject:[NSValue valueWithRange:NSMakeRange([((NSNumber*)item[@"location"]) intValue], [((NSNumber*)item[@"length"]) intValue])]];
        }
        
     _paragraph  = [WordParse createDetail:_content[@"content"] tappedRange:ranges];
        _textView.attributedText = _paragraph;
        
    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)delete:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"確認" message:@"この問題を消去しますか?" delegate:self cancelButtonTitle:@"キャンセル" otherButtonTitles:@"はい", nil];
 
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    PFQuery *query = [PFQuery queryWithClassName:@"Answer"];
    switch(buttonIndex){
        case 0:
            
            break;
            
        case 1:
            [query whereKey:@"question" equalTo:_question];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                for (PFObject *item in objects) {
                    [item deleteInBackground];
                }
            }];
            [_question delete];
            [self.delegate deleteComplete:_question];
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
    
    }

}


@end
