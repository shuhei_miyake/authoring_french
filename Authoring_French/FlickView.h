//
//  FlickView.h
//  French_practice
//
//  Created by Shuhei on 2014/10/31.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickView : UIView

@property NSString *word;
@property UITextView *textView;
@property NSRange range;

-(id)initWithFrame:(NSUInteger)height Word:(NSString *)Word tappedRange:(NSRange)range;
-(NSUInteger)closestCharactor:(CGPoint)point;
-(void)toggleCharColor:(NSRange)range;
@end
