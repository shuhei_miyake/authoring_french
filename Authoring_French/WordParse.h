//
//  WordParse.h
//  French_practice
//
//  Created by Shuhei on 2014/10/24.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface WordParse : NSObject


+(NSMutableAttributedString *)parseWordtoAttributedString:(NSString *)string;

+(NSMutableAttributedString*)splitWord:(NSString *)string tappedRange:(NSRange)range;
+(NSMutableAttributedString*)createDetail:(NSString *)string tappedRange:(NSArray*)ranges;
@end
