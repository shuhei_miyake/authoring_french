//
//  ViewController.m
//  French_practice
//
//  Created by Shuhei on 2014/10/24.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "HintViewController.h"
#import "QuestionTableViewController.h"
#import "ConfirmHintViewController.h"
#import "CreateHintViewController.h"
@interface ViewController ()


@end


@implementation ViewController


@synthesize flickview;
@synthesize tapStart;
//フリックスタート位置を認識したフラグ
int flag = 0;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //初期化
    _Words = [NSMutableArray new];
    _paragraph.attributedText = [WordParse parseWordtoAttributedString:_content[@"content"]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textTapped:)];
    UILongPressGestureRecognizer *longpress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(textLongPressed:)];
    
    [_paragraph addGestureRecognizer:tap];
    [_paragraph addGestureRecognizer:longpress];
    
    _questions = [NSMutableArray new];
    _answers = [NSMutableArray new];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//テキストタップ時の処理
- (void)textTapped:(UITapGestureRecognizer *)recognizer
{
    [_question_field endEditing:YES];
    //テキストのattributeを取得
    NSDictionary *attributes = [self GetAttributedStringDictionary:recognizer];
    //単語取得
    NSString *text = [attributes objectForKey:@"text"];
    if([text isEqualToString:@" "] || [text isEqualToString:@"/"] || [text isEqualToString:@"//"]){
        return;
    }
    
    //テキスト内の単語のrange
    NSRange range = [[attributes objectForKey:@"range"] rangeValue];
    
    if([attributes objectForKey:@"ParentRange"]){
        range = [[attributes objectForKey:@"ParentRange"] rangeValue];
    }
    
    //すでに選択されている単語内のrange
    NSRange wordRange = [[attributes objectForKey:@"isTapped"] rangeValue];
    
    UIFont *font = [UIFont fontWithName:nil size:30.f];
    
    
  //  _paragraph.scrollEnabled = NO;
    
    //全て選択されていなければ全選択
    [_paragraph.textStorage beginEditing];
    if (wordRange.length != text.length){
        
        [_paragraph.textStorage addAttributes:@{@"text":text,
                                                NSFontAttributeName:font,
                                                NSForegroundColorAttributeName:[UIColor redColor],
                                                @"isTapped":[NSValue valueWithRange:NSMakeRange(0, text.length)]} range:NSMakeRange(range.location, text.length)];
        
        
        
    }else{
        //全て選択されていれば全解除
        [_paragraph.textStorage addAttributes:@{@"text":text,
                                                NSFontAttributeName:font,
                                                NSForegroundColorAttributeName:[UIColor blackColor],
                                                @"isTapped":[NSValue valueWithRange:NSMakeRange(0,0)]} range:NSMakeRange(range.location, text.length)];
    }
    
  [_paragraph.textStorage endEditing];
    
  //  _paragraph.scrollEnabled = YES;
}


//長押し時の処理
-(void)textLongPressed:(UILongPressGestureRecognizer *)recognizer{
    //長押し開始時
    if([recognizer state]==UIGestureRecognizerStateBegan){
        NSDictionary *attributes = [self GetAttributedStringDictionary:recognizer];
        NSString *text = [attributes objectForKey:@"text"];
    if([text isEqualToString:@" "] || [text isEqualToString:@"/"] || [text isEqualToString:@"//"]){
        return;
    }
        NSRange wordRange = [[attributes objectForKey:@"isTapped"] rangeValue];
        CGPoint superLocation = [recognizer locationInView:self.view];
        //フリック生成、描画
        flickview = [[FlickView alloc]initWithFrame:superLocation.y Word:text tappedRange:wordRange];
        [self.view addSubview:flickview];
        
        //フリック入力中の単語を記憶
        tapStart =[[attributes objectForKey:@"range"] rangeValue];
        
        //フリック中の処理
    }else if([recognizer state]==UIGestureRecognizerStateChanged){
        
        if(CGRectContainsPoint(flickview.frame, [recognizer locationInView:self.view])){
            
            if(flag == 0){
                //最初の文字選択
                CGPoint location = [recognizer locationInView:self.view];
                //最初の文字の位置を記憶
                _start = [flickview closestCharactor:CGPointMake(location.x-15, 20)];
                
                //フラグ立てる
                flag = 1;
            }
            
            if(flag == 1){
            //フリックで触った文字色を変化
            CGPoint location = [recognizer locationInView:self.view];
            NSUInteger tmp =  [flickview closestCharactor:CGPointMake(location.x, 20)];
            if(tmp > _start){
                [flickview toggleCharColor:NSMakeRange(_start, tmp -_start+1)];
            }else{
                [flickview toggleCharColor:NSMakeRange(tmp, _start-tmp+1)];
            }
            
            
                _end = [flickview closestCharactor:CGPointMake(location.x, 20)];
            }
            
            if(flag == 2){
            
                flag = 0;
            }
            
        }else{
            if(flag == 1){
                flag = 2;
            }
        }
        
        
        
        //長押し終了時の処理
    }else if([recognizer state]==UIGestureRecognizerStateEnded){
        
        [flickview removeFromSuperview];
        
        //フリック入力開始している時
        if(flag != 0){
            //スタートより前をフリックした場合、スタートとエンドを入れ替える
            if(_start>_end){
                NSUInteger tmp = _end;
                _end = _start;
                _start = tmp;
            }
            
            
            //選択する文字のrange → 記憶しておいた単語の位置にフリックで選択したrangeを足し合わせる
            NSRange tappedrange = NSMakeRange(_start, _end-_start+1);
            NSDictionary *attributes = [_paragraph.textStorage attributesAtIndex:tapStart.location longestEffectiveRange:nil inRange:tapStart];
            NSString *text = [attributes objectForKey:@"text"];
            if([attributes objectForKey:@"ParentRange"]){
                tapStart = [[attributes objectForKey:@"ParentRange"] rangeValue];
            }
            UIFont *font = [UIFont fontWithName:nil size:30.f];
            
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:@{@"text":text,
                                                                                                                    NSFontAttributeName:font,
                                                                                                                    NSForegroundColorAttributeName:[UIColor blackColor],
                                                                                                                    @"ParentRange":[NSValue valueWithRange:tapStart],
                                                                                                                    @"isTapped":[NSValue valueWithRange:tappedrange]}];
            
            [string addAttribute:@"isTapped" value:[NSValue valueWithRange:tappedrange] range:tappedrange];
            [string addAttribute:@"text" value:text range:tappedrange];
            [string addAttribute:@"ParentRange" value:[NSValue valueWithRange:tapStart] range:tappedrange];
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:tappedrange];
            
           // _paragraph.scrollEnabled = NO;
            [_paragraph.textStorage beginEditing];
            [_paragraph.textStorage replaceCharactersInRange:tapStart withAttributedString:string];
            [_paragraph.textStorage endEditing];
          //  _paragraph.scrollEnabled = YES;
            flag = 0;
            
        }
    }
}

//タップした単語のアトリビュートを取得
-(NSDictionary*)GetAttributedStringDictionary:(UIGestureRecognizer *)recognizer{
    
    UITextView *textView = (UITextView *)recognizer.view;
    NSMutableDictionary *attributes;
    // Location of the tap in text-container coordinates
    
    NSLayoutManager *layoutManager = textView.layoutManager;
    CGPoint location = [recognizer locationInView:textView];
    location.x -= textView.textContainerInset.left;
    location.y -= textView.textContainerInset.top;
    
    // Find the character that's been tapped on
    
    NSUInteger characterIndex;
    characterIndex = [layoutManager characterIndexForPoint:location
                                           inTextContainer:textView.textContainer
                  fractionOfDistanceBetweenInsertionPoints:NULL];
    
    NSRange range;
    if (characterIndex < textView.textStorage.length) {
        attributes =  [[textView.textStorage attributesAtIndex:characterIndex effectiveRange:&range] mutableCopy];
        //その単語のrangeをdictionaryに入れて返す
        [attributes setObject:[NSValue valueWithRange:range] forKey:@"range"];
        
        
    }
    return attributes;
    
}


- (IBAction)toNextInput:(id)sender {
    
    UITabBarController *tabBar = [self.storyboard instantiateViewControllerWithIdentifier:@"Confirmation"];
    
    CreateHintViewController *hint = [[self.tabBarController viewControllers] objectAtIndex:1];
    
    HintViewController *view  = [[tabBar viewControllers] objectAtIndex:0];
    view.delegate = self;
    view.paragraph = [[_paragraph textStorage] mutableCopy];
    view.question = _question_field.text;
    view.content = self.content;
    view.grammer = hint.grammer;

    ConfirmHintViewController *view2 = [[tabBar viewControllers] objectAtIndex:1];
    
    
    view2.hint_string = hint.hint_text.text;
    view2.description_string = hint.description_text.text;
    view2.grammer_string = hint.grammer[@"name"];

    
    
    [self presentViewController:tabBar animated:YES completion:nil];
    
    
}

-(void)finishConfirm{
    
    _paragraph.attributedText = [WordParse parseWordtoAttributedString:_content[@"content"]];
    _question_field.text = @"";
    
    CreateHintViewController *createhint = [[self.tabBarController viewControllers] objectAtIndex:1];
    createhint.hint_text.text = @"";
    createhint.description_text.text = @"";

}


- (IBAction)toQuestionVIew:(id)sender {
    QuestionTableViewController *view =[self.storyboard instantiateViewControllerWithIdentifier:@"questionTable"];
    view.content = _content;
    [self presentViewController:view animated:YES completion:nil];
    
}
@end
