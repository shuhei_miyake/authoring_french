//
//  CreateHintViewController.m
//  Authoring_French
//
//  Created by Shuhei on 2014/11/27.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import "CreateHintViewController.h"

@interface CreateHintViewController ()

{

    NSMutableArray *grammers;
    
}

@end

@implementation CreateHintViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _picker.dataSource =self;
    _picker.delegate=self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(endEdit:)];
    
    [self.view addGestureRecognizer:tap];
    
    grammers = [NSMutableArray new];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Grammer"];

    [query orderByDescending:@"UpdatedAt"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        grammers = [objects mutableCopy];
        [_picker reloadAllComponents];
        
    }];
    
    
}

-(void)endEdit:(UITapGestureRecognizer *)recognizer{

    [_description_text endEditing:YES];
    [_hint_text endEditing:YES];
    

}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{

    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

    return grammers.count;

}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    PFObject *item = grammers[row];
    
    return item[@"name"];
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{

    _grammer = grammers[row];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
