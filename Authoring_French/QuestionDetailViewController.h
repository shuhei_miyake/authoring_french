//
//  QuestionDetailViewController.h
//  French_practice
//
//  Created by Shuhei on 2014/11/22.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@protocol QuestionDetailViewControllerDelegate <NSObject>

@optional
-(void)deleteComplete:(PFObject*)question;

@end


@interface QuestionDetailViewController : UIViewController
<UIAlertViewDelegate>

@property PFObject *content;
@property PFObject *question;
@property NSMutableAttributedString *paragraph;
@property id<QuestionDetailViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *question_label;
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)back:(id)sender;
- (IBAction)delete:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *hint;
@property (weak, nonatomic) IBOutlet UITextView *description_view;

@end
