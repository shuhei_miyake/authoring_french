//
//  YSTapUpDownGestureRecognizer.h
//  French_practice
//
//  Created by Shuhei on 2014/10/27.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YSTapUpDownGestureRecognizer;
typedef void(^YSTapUpDownGestureRecognizerTouchesDown) (YSTapUpDownGestureRecognizer *tapUpDownGesture, NSSet *touches, UIEvent *event);
typedef void(^YSTapUpDownGestureRecognizerTouchesUp) (YSTapUpDownGestureRecognizer *tapUpDownGesture, NSSet *touches, UIEvent *event, BOOL isTouchUpInside);
typedef void(^YSTapUpDownGestureRecognizerTouchesMove) (YSTapUpDownGestureRecognizer *tapUpDownGesture, NSSet *touches, UIEvent *event, CGPoint touchPoint);


@interface YSTapUpDownGestureRecognizer : UIGestureRecognizer

- (id)initWithTouchDown:(YSTapUpDownGestureRecognizerTouchesDown)touchDown touchUp:(YSTapUpDownGestureRecognizerTouchesUp)touchUp touchMoved:(YSTapUpDownGestureRecognizerTouchesMove)touchMove;

@end
