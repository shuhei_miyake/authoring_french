//
//  YSTapUpDownGestureRecognizer.m
//  French_practice
//
//  Created by Shuhei on 2014/10/27.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import "ViewController.h"
#import "YSTapUpDownGestureRecognizer.h"
@interface YSTapUpDownGestureRecognizer ()

@property (nonatomic, copy) YSTapUpDownGestureRecognizerTouchesDown touchDown;
@property (nonatomic, copy) YSTapUpDownGestureRecognizerTouchesUp touchUp;
@property (nonatomic, copy) YSTapUpDownGestureRecognizerTouchesMove touchMove;

@end

@implementation YSTapUpDownGestureRecognizer

- (id)initWithTouchDown:(YSTapUpDownGestureRecognizerTouchesDown)touchDown touchUp:(YSTapUpDownGestureRecognizerTouchesUp)touchUp touchMoved:(YSTapUpDownGestureRecognizerTouchesMove)touchMove
{
    if (self = [super init]) {
        self.touchDown = touchDown;
        self.touchUp = touchUp;
        self.touchMove = touchMove;
    }
    return self;
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint touchpoint = [touch locationInView:self.view];
    
    if (self.touchMove) self.touchMove(self,touches,event,touchpoint);

}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.touchDown) self.touchDown(self, touches, event);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    /* 指が離れた位置が対象のビュー内か(UIControlEventTouchUpInsideを判定) */
    UITouch *touch = [touches anyObject];
    
    CGPoint touchPoint = [touch locationInView:self.view];
    CGRect threshold = CGRectMake(touchPoint.x, touchPoint.y, 10, 10);
    BOOL isTouchUpInside = CGRectContainsPoint(threshold, touchPoint);
    
    if (self.touchUp) self.touchUp(self, touches, event, isTouchUpInside);
}

@end
