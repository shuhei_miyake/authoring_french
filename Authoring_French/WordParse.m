//
//  WordParse.m
//  French_practice
//
//  Created by Shuhei on 2014/10/24.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import "WordParse.h"

@implementation WordParse

//テキストを単語ごとに配列に格納
+(NSArray*)parseWordtoArray:(NSString *)string{
    NSArray *lines = [string componentsSeparatedByString:@" "];
    return lines;
}

//単語を文字ごとに分割
+(NSArray*)parseWordtoCharacter:(NSString *)string{
    NSMutableArray *lines = [NSMutableArray new];
    NSMutableString *tmp = [string mutableCopy];
    while (tmp.length>0) {
        [lines addObject:[tmp substringToIndex:1]];
        [tmp deleteCharactersInRange:NSMakeRange(0, 1)];
    }
    
    return lines;
}


//テキストをattributedstringに変換
+(NSMutableAttributedString*)parseWordtoAttributedString:(NSString *)string{
    NSArray *Words = [self parseWordtoArray:string];
    NSMutableAttributedString *paragraph = [[NSMutableAttributedString alloc]init];
    //フォントサイズ
    UIFont *font = [UIFont fontWithName:nil size:30.f];
    
    for(NSString *item in Words){
    
        //単語のあトリビュート
        NSAttributedString* attrubutedString = [[NSAttributedString alloc] initWithString:item attributes:@{@"text":item,
                                                                                                            NSFontAttributeName:font,
                                                                                                            @"isTapped":[NSValue valueWithRange:NSMakeRange(0, 0)]}];
        
    [paragraph appendAttributedString:attrubutedString];
        //空白文字のアトリビュート
        NSAttributedString* space = [[NSAttributedString alloc] initWithString:@" " attributes:@{@"text":@" ",
                                                                                                 NSFontAttributeName:font,
                                                                                                 @"isTapped":[NSValue valueWithRange:NSMakeRange(0, 0)]}];
        
        [paragraph appendAttributedString:space];
        
    }
    
    
    return paragraph;
}

//単語をattributedstringに変換(フリックビュー用)
+(NSMutableAttributedString*)splitWord:(NSString *)string tappedRange:(NSRange)range{

    NSArray *Words = [self parseWordtoCharacter:string];
    NSMutableAttributedString *paragraph = [[NSMutableAttributedString alloc]init];
    //文字サイズ
    UIFont *font = [UIFont fontWithName:nil size:50.f];
    
    for(NSString *item in Words){
    
        NSAttributedString* attributedString = [[NSAttributedString alloc] initWithString:item attributes:@{@"text":item,
                                                                                                            NSFontAttributeName:font,
                                                                                                            NSKernAttributeName:@10.0f //文字間隔
                                                                                                            }];
        
    [paragraph appendAttributedString:attributedString];
        
    }
    
    [paragraph addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
    return paragraph;

}

+(NSMutableAttributedString*)createDetail:(NSString *)string tappedRange:(NSArray*)ranges{
    NSMutableAttributedString *value = [[NSMutableAttributedString alloc]initWithString:string];
    
    for(NSValue *range in ranges){
    
        [value setAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} range:[range rangeValue]];
    }
    
    return value;
    
}


@end
