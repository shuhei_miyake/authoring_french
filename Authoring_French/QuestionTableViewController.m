//
//  QuestionTableViewController.m
//  French_practice
//
//  Created by Shuhei on 2014/11/22.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import "QuestionTableViewController.h"
@interface QuestionTableViewController ()

@end

@implementation QuestionTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tableView.dataSource = self;
    _tableView.delegate =self;
    
    PFQuery *query = [PFQuery queryWithClassName:@"Question"];
    [query whereKey:@"content" equalTo:_content];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects,NSError *error){
    
        if(error){
            NSLog(@"エラー");
        }else{
        
            _questions = [objects mutableCopy];
            [_tableView reloadData];
            
        }
        
    }];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return _questions.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // セルが作成されていないか?
    if (!cell) { // yes
        // セルを作成
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    PFObject *item = [_questions objectAtIndex:indexPath.row];
    cell.textLabel.text = item[@"question"];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    QuestionDetailViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"detailView"];
    view.question = [_questions objectAtIndex:indexPath.row];
    view.content = _content;
    view.delegate = self;
    [self presentViewController:view animated:YES completion:nil];

}

-(void)deleteComplete:(PFObject *)question{
    [_questions removeObject:question];
    [_tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
