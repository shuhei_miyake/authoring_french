//
//  ConfirmViewController.m
//  French_practice
//
//  Created by Shuhei on 2014/11/22.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import "HintViewController.h"
#import "ConfirmHintViewController.h"
@interface HintViewController ()

@end

@implementation HintViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _textView.attributedText = self.paragraph;
    _question_label.text = self.question;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)confirm:(id)sender {
    NSMutableAttributedString *string = [[_textView textStorage] mutableCopy];
    PFObject *question = [PFObject objectWithClassName:@"Question"];
    question[@"content"] = _content;
    question[@"question"] = _question_label.text;
    question[@"description"] = [(ConfirmHintViewController*)[[self.tabBarController viewControllers] objectAtIndex:1] description_string];
    question[@"hint"] = [(ConfirmHintViewController*)[[self.tabBarController viewControllers] objectAtIndex:1] hint_string];
    
    PFRelation *question_relation = [question relationForKey:@"Question_Grammer"];
    [question_relation addObject:_grammer];
    
    PFACL *acl = [PFACL ACL];
    [acl setPublicReadAccess:YES];
    [question setACL:acl];
    
    [question save];
    
    
    
    [string enumerateAttribute:@"isTapped" inRange:NSMakeRange(0, string.length)  options:nil usingBlock:^(id value, NSRange range,BOOL *strop){
        
        NSString *rangeString = [NSString stringWithFormat:@"%@",value];
        NSRange newrange = NSRangeFromString(rangeString);
        if(newrange.length > 0){
            PFObject *answer = [PFObject objectWithClassName:@"Answer"];
            answer[@"question"] = question;
            answer[@"location"] =  [NSNumber numberWithInteger:newrange.location+range.location];
            answer[@"length"] =[NSNumber numberWithInteger:newrange.length];
            [answer setACL:acl];
            [answer saveInBackground];
        }
        
    }];
    [self.delegate finishConfirm];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
