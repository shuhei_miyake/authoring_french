//
//  QuestionTableViewController.h
//  French_practice
//
//  Created by Shuhei on 2014/11/22.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "QuestionDetailViewController.h"

@interface QuestionTableViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate,QuestionDetailViewControllerDelegate>

@property PFObject *content;
@property NSMutableArray *questions;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)back:(id)sender;


@end
