//
//  CreateHintViewController.h
//  Authoring_French
//
//  Created by Shuhei on 2014/11/27.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HintViewController.h"
#import <Parse/Parse.h>

@interface CreateHintViewController : UIViewController
<HintViewControllerProtocol,UIPickerViewDataSource,UIPickerViewDelegate>
@property PFObject *grammer;
@property (weak, nonatomic) IBOutlet UITextView *hint_text;
@property (weak, nonatomic) IBOutlet UITextView *description_text;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
 
@end
