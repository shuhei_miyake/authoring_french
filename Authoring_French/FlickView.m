//
//  FlickView.m
//  French_practice
//
//  Created by Shuhei on 2014/10/31.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import "FlickView.h"
#import "WordParse.h"

@implementation FlickView
@synthesize word;
@synthesize textView;
@synthesize range;

//初期化
-(id)initWithFrame:(NSUInteger)height Word:(NSString *)Word tappedRange:(NSRange)Range{
    
    if(self = [super init]){
        
        self.word = Word;
        //マジックナンバー
        self.frame = CGRectMake(0, height-90, 320.0f, 66.0f);
        self.range = Range;
        
        //マジックナンバー
        textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, 320.f, 66.0f)];
        textView.editable = NO;
        //単語を文字単位に分割
        NSAttributedString *text = [WordParse splitWord:word tappedRange:range];
        textView.attributedText = text;
        
        //中央揃え
        [textView setTextAlignment:NSTextAlignmentCenter];
        
        [self addSubview:textView];
        
        
    };
    
    
    return self;
    
}


//フリック時に最も近い位置にある文字の場所を返す
-(NSUInteger)closestCharactor:(CGPoint)point{
    point.x -= 20;
    UITextPosition *beginning = textView.beginningOfDocument;
    UITextPosition *end = [textView closestPositionToPoint:point];
    UITextRange *Range = [textView textRangeFromPosition:beginning toPosition:end];
    textView.selectedTextRange = Range;
    
    NSUInteger pos = [textView offsetFromPosition:beginning toPosition:end];
    if(pos==word.length){
        pos = word.length-1;
    }
    return pos;
}

//文字色を変化させる
-(void)toggleCharColor:(NSRange)Range{
    NSMutableAttributedString *string = [textView.textStorage mutableCopy];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, word.length)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:Range];
    
    textView.attributedText = string;
    
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
