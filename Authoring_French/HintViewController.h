//
//  ConfirmViewController.h
//  French_practice
//
//  Created by Shuhei on 2014/11/22.
//  Copyright (c) 2014年 Shuhei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@protocol HintViewControllerProtocol <NSObject>

@optional
-(void)finishConfirm;


@end


@interface HintViewController : UIViewController
@property (nonatomic,assign) id<HintViewControllerProtocol> delegate;
@property (weak, nonatomic) IBOutlet UILabel *question_label;
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)back:(id)sender;
- (IBAction)confirm:(id)sender;

@property NSMutableAttributedString *paragraph;
@property NSString *question;
@property PFObject *content;
@property PFObject *grammer;

@end
